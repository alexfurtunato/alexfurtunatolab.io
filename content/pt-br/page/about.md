---
title: Sobre mim
subtitle: Algumas poucas palavras sobre mim
comments: false
---

Meu nome é Alex Furtunato. E eu moro na região nordeste do Brasil, na cidade denominada Natal. Minha cidade é um belo lugar aonde o verão dura quase todo o ano. Nós temos muitas praias com um mar na cor verde.

- Eu gosto de praticar stand up paddle
- Atualmente estou aprendendo a velejar
- E, meu hobby quando estou em minha casa é trabalhos de marcenaria e com couro

### meu currículo resumido

Minha graduação foi em Engenharia Elétrica e mestrado em Engenharia Elétrica e de Computação, mas, na verdade eu sempre trabalhei na área de Tecnologia da Informação, como professor e Gerenciamento de TI.

#### Experiência

**Instituto Federal do Rio Grande do Norte**

- Professor
  - Agosto 1997 – Presente
  - Professor de Redes de Computadores, especificamente nas áreas de gerenciamento de redes, devops e programação em Python.

- Diretor de TI
  - 1999 – Abril 2016
  - Gerenciamento de TI, Projeto e Infraestrutura de Datacenter, Virtualização (VMware), Serviços de Comunicação Unificada, Microsoft Sharepoint, Exchange, Lync, Sistemas Linux

#### Habilidades

- Servidores Linux e Windows
- VMware Vsphere e VCenter
- Containers Docker and LXC
- Devops
- Programação Python
- Gerenciamento e Administração de Redes de Computadores
- Kubernetes