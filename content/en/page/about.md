---
title: About me
subtitle: A few words about me
comments: false
---

My name is Alex Furtunato. And I live on Brasil northeast region, on the city named Natal. My city is a beautiful place where the summer lasts for most of the year. We have a lot of beaches with the green sea.

- I like to practice stand up paddle
- Currently, I'm learning how to sail
- And, my hobby at home is woodwork and leather jobs

### my summarized curriculum

My graduation was on Electrical Engineering and Master degree on Electrical and Computer Engineering, but, actually all of my positions always was on Technology Information area, with teaching and IT management jobs.

#### Experience

**Instituto Federal do Rio Grande do Norte**

- Teacher
  - Aug 1997 – Present
  - Networking teacher, specifically in the areas of network management, devops and python programming

- Chief Information Officer
  - 1999 – Apr 2016
  - IT Management, Datacenter Infrastructure Project, Virtualization (VMware), Unified Communication Services, Microsoft Sharepoint, Exchange, Lync Servers, Linux Systems

#### Skils

- Linux and Windows Servers
- VMware Vsphere and VCenter
- Docker and LXC Containers
- Devops
- Python progrmamming
- Network Administration and Management
- Kubernetes